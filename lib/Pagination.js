'use strict';

class Pagination {
  constructor(db, {
    fields = '*',
    page = 1,
    perPage = 10,
    sortBy = '',
    sortDir = 'ASC',
    search = '',
    searchBy = '',
    searchPattern = 'true',
    filters = ''
  }) {
    this._db = db;

    this._fields = [];

    this._table = '';

    this._where = '';

    this._groupBy = '';

    this._having = '';

    this._orderBy = '';

    this.fields = fields;

    this.page = Number(page);

    this.perPage = Number(perPage);

    this.sortBy = sortBy;

    this.sortDir = sortDir;

    this.search = search;

    this.searchBy = searchBy;

    this.searchPattern = searchPattern;

    this.filters = filters;
  }

  _initialize() {
    if(this.search && this.searchBy)
      switch(this.searchPattern) {
        case 'true':
          if(this._where) this._where += ` AND ${this.searchBy} LIKE '%${this.search}%'`;
          else this._where = `WHERE ${this.searchBy} LIKE '%${this.search}%'`;

          break;
        case 'false':
          if(this._where) this._where += ` AND ${this.searchBy} = ${isNaN(this.search) ? '\'' + this.search + '\'' : this.search}`;
          else this._where = `WHERE ${this.searchBy} = ${isNaN(this.search) ? '\'' + this.search + '\'' : this.search}`;

          break;
        default:
          break;
      }


    if(this.sortBy) this._orderBy = `ORDER BY ${this.sortBy} ${this.sortDir}`;

    if(this.filters) {
      try {
        const filters = JSON.parse(this.filters);

        Object.keys(filters).forEach(key => {
          const where = filters[key].replace(/\,/g, ` OR ${key} = `)

          this.where(`(${key} = ${where})`);
        });
      }catch(err) {};
    }
  }

  async exec() {
    this._initialize();

    const count = await this._db.raw(`
      SELECT COUNT(*) AS total
      FROM ${this._table}
      ${this._where}
      ${this._groupBy}
      ${this._having};
    `);

    const total = this._groupBy ? count[0].length : count[0][0].total;

    const offset = (this.page - 1) * this.perPage;

    const lastPage = total > 0 ? Math.ceil(total / this.perPage) : 1;

    const result = await this._db.raw(`
      SELECT
        ${this._fields.join(',')}
      FROM ${this._table}
      ${this._where}
      ${this._groupBy}
      ${this._having}
      ${this._orderBy}
      LIMIT ${this.perPage} OFFSET ${offset};
    `);

    const data = result[0];

    return {
      total,
      data,
      page: this.page,
      per_page: this.perPage,
      pagination: {
        first: 1,
        last: lastPage,
        previous: this.page > 1 ? this.page - 1 : null,
        next: this.page < lastPage ? this.page + 1 : null
      }
    };
  }

  select() {
    this._fields = [].concat(...arguments);

    if(!this._fields.length)
      this._fields = this.fields.split(',');

    const self = this;

    delete self.select;

    return self;
  }

  from(table) {
    this._table = table;

    const self = this;

    delete self.from;

    return self;
  }

  where(values) {
    switch(typeof values) {
      case 'string':
        if(this._where) this._where += ` AND ${values}`;
        else this._where = `WHERE ${values}`;

        break;
      case 'object':
        Object.keys(values).forEach(key => {
          const value = values[key];

          if(this._where) this._where += ` AND ${key} = ${isNaN(value) ? '\'' + value + '\'' : value}`;
          else this._where = `WHERE ${key} = ${isNaN(value) ? '\'' + value + '\'' : value}`;
        });

        break;
      default:
        break;
    }

    return this;
  }

  groupBy(groupBy) {
    this._groupBy = 'GROUP BY ' + groupBy;

    const self = this;

    delete self.groupBy;

    return self;
  }

  having(values) {
    switch(typeof values) {
      case 'string':
        if(this._having) this._having += ` AND ${values}`;
        else this._having = `WHERE ${values}`;

        break;
      case 'object':
        Object.keys(values).forEach(key => {
          const value = values[key];

          if(this._having) this._having += ` AND ${key} = ${isNaN(value) ? '\'' + value + '\'' : value}`;
          else this._having = `WHERE ${key} = ${isNaN(value) ? '\'' + value + '\'' : value}`;
        });

        break;
      default:
        break;
    }

    return this;
  }

  orderBy(sortBy, sortDir = 'ASC') {
    if(this._orderBy) this._orderBy += `, ${sortBy} ${sortDir}`;
    else this._orderBy = `ORDER BY ${sortBy} ${sortDir}`;

    return this;
  }
}

module.exports = Pagination;
