module.exports = {
  'root': true,
  'extends': 'eslint-config-egg',
  'rules': {
    'curly': ['error', 'multi'],
    'comma-dangle': ['error', 'never'],
    'keyword-spacing': ['error', {
      'overrides': {
        'if': {
          'before': false,
          'after': false
        },
        'else': {
          'before': false,
          'after': true
        },
        'for': {
          'before': false,
          'after': false
        },
        'while': {
          'before': false,
          'after': false
        },
        'switch': {
          'before': false,
          'after': false
        },
        'catch': {
          'before': false,
          'after': false
        }
      }
    }]
  }
};
