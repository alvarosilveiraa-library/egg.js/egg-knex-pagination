'use strict';

const Pagination = require('./lib/Pagination');

module.exports = app => {
  app.coreLogger.info('[egg-knex-pagination] egg-knex-pagination begin start');

  const start = Date.now();

  app.knex.pagination = (options = {}) => {
    return new Pagination(app.knex, options);
  }

  app.coreLogger.info('[egg-knex-pagination] egg-knex-pagination started use %d ms', Date.now() - start);
};
